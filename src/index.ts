import { Client, Module, ModuleManifest } from "discord-modules";
import { ThreadCreateListener } from "listeners/ThreadCreateListener";

export default class ThreadModule extends Module {
    constructor(client: Client, manifest: ModuleManifest) {
        super(client, manifest);
        console.info("INFO: Constructed discord-module-threads.");
    }

    public async register(): Promise<void> {
        console.info("INFO: Registered discord-module-threads.");

        await Promise.all([
            this.client.listenerManager.register(new ThreadCreateListener(this))
        ]).catch(console.error);

        return;
    }
}