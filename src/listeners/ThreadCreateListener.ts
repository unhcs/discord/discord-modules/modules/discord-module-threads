import { 
  Module,
  Listener 
} from "discord-modules";
import { ThreadChannel } from "discord.js";

export class ThreadCreateListener extends Listener<"threadCreate"> {
  constructor(module: Module) {
      super(module, "threadCreate");

      console.info("INFO: Constructed discord-module-threads ThreadCreateListener.");
  }

  public async execute(thr: ThreadChannel): Promise<void> {
    console.log(`Thread created at ${thr.createdAt}`);
    thr.setAutoArchiveDuration(4320) //Three Days
  }
}